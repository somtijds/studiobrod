<?php
/**
 * Function loader for Studio Brod / Oliver Brod
 * Author: Willem Prins | SOMTIJDS
 * Author URI: http://www.somtijds.de
 * Project: Studio-Oliver-Brod
 * Date created: 08/05/2017
 *
 * @package Studio Oliver Brod
 * @since Studio Oliver Brod 2.0
 **/

// Add legacy theme functions (some might be commented out).
require_once( get_template_directory() . '/functions/twentyten.php' );

// Add shortcode functions.
require_once( get_template_directory() . '/functions/shortcodes.php' );

// Add script/style enqueueing
require_once( get_stylesheet_directory() . '/functions/enqueue-scripts.php' );

// Add template functions
require_once( get_template_directory() . '/functions/template-functions.php' );

// Add metabox functions when CMB2 class exists / plugin is installed.
if ( is_admin() && class_exists( 'CMB2' ) ) {
	require_once( get_stylesheet_directory() . '/functions/admin-cmb2.php' ); 
}