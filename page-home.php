<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<div class="page-web page-web--home">
	<a id="vu" href="<?php echo get_site_url(); ?>"></a>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>

	<?php endwhile; ?>
</div>

<?php get_footer(); ?>