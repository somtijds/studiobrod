<?php
/**
 * Shortcodes for Studio Brod / Oliver Brod (given its own file by WP)
 * Author: Florian Sarges
 * Author URI: http://www.flowtographyberlin.de/
 * Project: Studio-Oliver-Brod
 * Date created: 08/05/2017
 *
 * @package Studio Oliver Brod
 * @since Studio Oliver Brod 2.0
 **/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
};

/**
 *
 * OLIVERBROD & STUDIOBROD CUSTOM SHORTCODES
 *
 */

function omachtgerade($atts, $content = null) {
	return '<p class="plusdrei">'.$content.'<a class="more-link" href="/aktuelles/">&hellip; weiterlesen</a></p>';
}
add_shortcode('machtgerade', 'omachtgerade');


function obutton($atts, $content = null) {
	extract(shortcode_atts(array(
		"link" => '#',
		"type" => '#'
	), $atts));
	return '<div class="buttoncontainer"><a class="downloadbutton '.$type.'" href="'.$link.'" target="_blank">Download</a><p class="buttontext">'.$content.'</p></div>';
}
add_shortcode('downloadbutton', 'obutton');


function oaudiotrack ($atts, $content = null) {
	extract(shortcode_atts(array(
		"title" => '#',
		"audiolink" => '#',
		"logolink" => '#'
	), $atts));
	return '<img class="logo" alt="" src="'.$logolink.'" /><div class="audiotrack"><a class="wpaudio" href="'.$audiolink.'">'.$title.'</a><p>'.$content.'</p></div><div class="clearer"></div>';
}
add_shortcode('hoerbeispiel', 'oaudiotrack');


function opageend () {
	return '<div class="clearer"></div>';
}
add_shortcode('seitenende', 'opageend');


function ored($atts, $content = null) {
	return '<span class="red">'.do_shortcode($content).'</span>';
}
add_shortcode('rot', 'ored');


function oorange($atts, $content = null) {
	return '<span class="orange">'.do_shortcode($content).'</span>';
}
add_shortcode('orange', 'oorange');


function ogrey($atts, $content = null) {
	return '<span class="grey">'.do_shortcode($content).'</span>';
}
add_shortcode('grau', 'ogrey');


function oblack($atts, $content = null) {
	return '<span class="black">'.do_shortcode($content).'</span>';
}
add_shortcode('schwarz', 'oblack');


function ocolumnleft($atts, $content = null) {
	return '<div class="column-left">'.do_shortcode($content).'</div>';
}
add_shortcode('linkespalte', 'ocolumnleft');

function ocolumnright($atts, $content = null) {
	return '<div class="column-right">'.do_shortcode($content).'</div><div class="clearer"></div>';
}
add_shortcode('rechtespalte', 'ocolumnright');


function oseo ($atts, $content = null) {
	return '<div class="garnichtda">'.do_shortcode($content).'</div>';
}
add_shortcode('seo', 'oseo');


/* 
 *
 * OTAB FUNCTIONS & SHORTCODE
 *
 */

function otabs($atts, $content = null) {
	return '<div class="otabs-container">'.do_shortcode($content).'</div>';
}
add_shortcode('tabs', 'otabs');

function otabsbar($atts, $content = null) {
	return '<ul class="otabs-nav">'.do_shortcode($content).'</ul>';
}
add_shortcode('tableiste', 'otabsbar');


function otab ($atts, $content = null) {
	extract(shortcode_atts(array(
		"name" => '#'
	), $atts));
	return '<li><a class="otab" href="#'.$name.'">'.$content.'</a></li>';
}
add_shortcode('tab', 'otab');


function otabinhalte($atts, $content = null) {
	return '<div class="otab-panel-container">'.do_shortcode($content).'</div>';
}
add_shortcode('tabinhalte', 'otabinhalte');


function otabinhalt ($atts, $content = null) {
	extract(shortcode_atts(array(
		"name" => '#'
	), $atts));
	return '<div class="otabpanel" id="'.$name.'">'.do_shortcode($content).'</div>';
}
add_shortcode('tabinhalt', 'otabinhalt');