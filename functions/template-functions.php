<?php
/**
 * Function loader for Studio Brod / Oliver Brod
 * Author: Willem Prins | SOMTIJDS
 * Author URI: http://www.somtijds.de
 * Project: Studio-Oliver-Brod
 * Date created: 25/07/2017
 *
 * @package Studio Oliver Brod
 * @since Studio Oliver Brod 2.0
 **/

/**
 * Display admin email encoded by EAE
 *
 * @return void
 * @author Willem Prins
 **/
function o20_email_address( $user_id = 1, $class = '' ) {

	$address = get_user_by( 'id',  $user_id )->user_email;

	if ( empty( $address ) || ! is_email( $address ) ) {
		return;
	}

	$mailto_address = $address;

	if ( ! empty( $link ) && function_exists('eae_encode_str') ) {
		$mailto_addres = eae_encode_str( 'mailto:' . $address );
		$address = eae_encode_str( $address );
	}
	$link = '<a href="' . $mailto_address . '" class="' . $class . '" title="' . __( 'Diese Link öffnet Ihren E-Mail-Programm', 'o20' ) . '">' . $address . '</a>';
	echo $link;
}

/**
 * Inline SVG from file
 *
 * @param string $svg_src Path to SVG file.
 * @return void.
 */
function o20_build_svg( $svg_src, $fallback = false) {
	if ( ! is_string( $svg_src ) || '' === $svg_src ) {
		return;
	}
	if ( ! is_readable( $svg_src ) ) {
		return;
	}
	$svg = file_get_contents( $svg_src );
	if ( $fallback ) {
		$png_src = str_replace( 'svg', 'png', $svg_src );
		$svg = o20_insert_svg_fallback( $svg, $png_src );
	}
	return $svg;
}

function o20_insert_svg_fallback( $svg, $png_src ) {
	// Add png fallback if available.
	if ( ! is_readable( $png_src ) ) {
		return $svg;
	} else {
		$png_insert = '<image src="' . $png_src . '" xlink:href=""></svg>';
		$svg = substr_replace( $svg, $png_insert, -6 );
		return $svg;
	}
}