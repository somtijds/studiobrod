<?php
/**
 * Enqueueing scripts and styles for Studio Brod / Oliver Brod
 * Author: Willem Prins | SOMTIJDS
 * Project: Studio-Oliver-Brod
 * Date created: 12/05/2017
 *
 * @package Studio Oliver Brod
 * @since Studio Oliver Brod 2.0
 **/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
};

add_action( 'wp_enqueue_scripts', 'o20_enqueue_scripts' );

if ( ! function_exists( 'o20_enqueue_scripts' ) ) {
	function o20_enqueue_scripts() {
		wp_enqueue_script('o20-modernizr', get_template_directory_uri().'/assets/js/lib/modernizr-o20.js', array(), false ); 
		wp_enqueue_script('jquerytools', get_template_directory_uri().'/assets/js/lib/jquery.tools.min.js', array('jquery'), '1.2.7', true ); 
		wp_enqueue_script('easytabs', get_template_directory_uri().'/assets/js/lib/jquery.easytabs.min.js', array('jquery'), '3.1', true ); 
		wp_enqueue_script('hashchange', get_template_directory_uri().'/assets/js/lib/jquery.hashchange.min.js', array('jquery'), '1.3', true );
		wp_enqueue_script('o20-scripts', get_template_directory_uri().'/assets/js/init.min.js', array('jquery'), true ); 
	}
}

add_action( 'wp_enqueue_scripts', 'o20_enqueue_styles' );

if ( ! function_exists( 'o20_enqueue_styles' ) ) {
	function o20_enqueue_styles() {
		wp_enqueue_style('o20-styles', get_template_directory_uri().'/assets/css/style.min.css', array(), false, 'all' ); 
	}
}