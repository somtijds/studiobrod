<?php
/**
 * Metaboxes for Studio Brod / Oliver Brod
 * Author: Willem Prins | SOMTIJDS
 * Project: Studio-Oliver-Brod
 * Date created: 08/05/2017
 *
 * @package Studio Oliver Brod
 * @since Studio Oliver Brod 2.0
 **/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
};