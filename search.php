<?php
/**
 * The template for displaying Search Results pages.
 */

get_header(); ?>

<div class="page-blog">

	<div class="blogcontainer">

		<?php if ( have_posts() ) : ?>
				<h3><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h3>
				<?php
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'search' );
				?>
	</div>

</div>

<?php get_footer(); ?>

		<?php else : ?>
					<h3>Play it again, Sam. (Danke.)</h3>
					<p>Leider konnte nichts gefunden werden, was sich hier glaubw&uuml;rdig als Suchergebnis pr&auml;sentieren lie&szlig;e. Versuchen Sie es doch noch einmal mit einem anderen Suchbegriff:</p>
					<div><?php get_search_form(); ?></div>
	</div>

</div>

<?php get_footer('search'); ?>
		<?php endif; ?>


