
var hasClass, addClass, removeClass, eventType, mobileMenu, navigation, navTrigger, toggleNav;

hasClass = function( elem, c ) {
	return elem.classList.contains( c );
};

addClass = function( elem, c ) {
	elem.classList.add( c );
};

removeClass = function( elem, c ) {
	elem.classList.remove( c );
};

toggleNav = function( event ) {
	event.preventDefault();
	toggleClass(navigation, navTrigger);
};

function toggleClass( elem, c ) {
	var fn = hasClass( elem, c ) ? removeClass : addClass;
	fn( elem, c );
}

eventType = Modernizr.touchevents ? 'touchstart' : 'click';

jQuery(document).ready( function( $ ) {

	if ( window.navigator.msMaxTouchPoints ) { 
		eventType = "pointerdown"; 
	}

	mobileMenu = document.querySelector('.o20_access-toggle'),
	navigation = document.querySelector('.menu'),
	navTrigger = 'menu-open';

	mobileMenu.addEventListener( eventType, toggleNav );

 	$('.o20_seitenwechsel a').mouseover(function(){
 		$(this).children('span').addClass('hover')
	;}).mouseout(function(){
		$(this).children('span').removeClass('hover');
	});

 	$('.o20_seitenwechsel a').tooltip({ 
 		position: 'bottom center', 
 		effect: 'slide', 
 		direction: 'left', 
 		slideOffset: -5
 	});

 	$('.otabs-container').easytabs();

 });