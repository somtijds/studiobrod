*** CHANGES TO CORE FILES: ***

wp-includes/formatting.php, line 181-183 
	commented out in order to prevent WP insert line breaks between anchor tags.
	
wp-includes/js/quicktags.js , added two buttons to HTML editor by:
	QTags.addButton( 'eg_paragraph', 'p', '<p>', '</p>', 'p' );QTags.addButton( 'eg_break', 'br', '', '<br />', 'br' );

wp-admin/css/colors-fresh.css , hide Update notifications added: 
	#adminmenu li span.update-plugins,.update-nag{display: none;}
	

*** CHANGES TO PLUGINS: ***

SOCIABLE:

Modified 
	#Facebook_Counter iframe{width:76px !important;}
in the sociable.css file to:
	#Facebook_Counter iframe{width:120px !important;}
	
Modified sociable_output.php:
	all <img> links to close with XHTML 1.0 compliant "/>" tags.


Video-JS:

Added 13px border, modified the Playbutton, modified text-color, focus states:

Added to .video-js:
	border:13px solid #000;margin-bottom:15px
Replaced playbutton with:
	.vjs-default-skin .vjs-big-play-button{display:block;z-index:2;position:absolute;top:50%;left:50%;width:8.0em;height:8.0em;margin:-42px 0 0 -42px;text-align:center;vertical-align:center;cursor:pointer!important;opacity:.95;-webkit-border-radius:15px;-moz-border-radius:15px;border-radius:15px;background:#ff2600;}

Added:
	.vjs-current-time-display,.vjs-remaining-time-display{color:#ff2600;}.video-js a:focus{outline:none;}

*** PLUGINS SETTINGS: ***

WP-AUDIO: 
FOnt-Size 16px, font-weight: bold.
Text-color, link-color, link-hover-color: inherit
Bar base: #eee
Bar load: #ccc
Bar-Position; Orange #ff7f00 or Red #ff2600, respectively.


Set SOCIABLE to 16px black icon version, below text, Blog index and Blog Page, no Pretext, no Sociable Link. 

EasyFancyBox
activated for Sections div.gallery-container only.
Transition In: Elastic Linear
Out: Elastic Swing



*** THEME REMARKS: ***

Excerpt Continue reading link has been hardcoded into functions.php file (i.e. will not be grabbed/adressed by languages file in case of translation).
Since excerpt function is only used for Search Results, not the Blog Index, the link defines the link displayed in search result excerpts only.