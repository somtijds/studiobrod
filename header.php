<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<meta name="google-site-verification" content="6TRIJbBN6cZ3wFcnm3IM0SwMmwbFf_LyXuHLVf9Ihw0" />
<meta name="viewport" content="initial-scale=1" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' );

	?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon" />

<?php wp_head();?>
</head>

<body <?php body_class(); ?>>

	<div class="o20_container">

		<header class="o20_header">

			<nav class="o20_home">
				<h1>
					<a href="<?php bloginfo('siteurl'); ?>" rel="home">studio<span class="black">.brod</span></a>
				</h1>
			</nav>

			<nav class="o20_nav-mobile">
				<button class="o20_access-toggle">
  					<?php echo o20_build_svg( get_stylesheet_directory() . '/assets/images/svg/settings.svg', true ); ?>
				</button>
			</nav>

			<nav class="o20_access">
				<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
				<?php wp_nav_menu( array( 'menu_class' => 'menu', 'theme_location' => 'primary' ) ); ?>
			</nav>

			<nav class="o20_seitenwechsel">
				<a href="http://www.oliverbrod.de/" title="seitenwechsel." target="_blank"><span class="links">oliver</span><span class="rechts">.brod</span></a>
			</nav>

		</header>