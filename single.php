<?php get_header(); ?>

<div class="page-blog">

	<div class="blogcontainer">

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

			<h3><?php the_title(); ?></h3>
			<div class="blog-column-left"><?php the_post_thumbnail('large'); ?></div>
			<div class="blog-column-right">
				<div class="text-blogpost">
					<?php the_content(); ?>
				</div>
			</div>
			<br class="clearer" />				
				<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>
			<div class="entry-prevnext-container">
				<span class="entry-prev">
						<?php previous_post_link( '%link', '' . _x( '&laquo;', 'Previous post link', 'twentyten' ) . ' %title' ); ?>
				</span>
				<span class="entry-next">
					<?php next_post_link( '%link', '%title ' . _x( '&raquo;', 'Next post link', 'twentyten' ) . '' ); ?>
				</span>
			</div>

		<?php endwhile; // end of the loop. ?>
		<br class="clearer" />
		<a class="backtooverview" href="/aktuelles/">Zur&uuml;ck zur News-&Uuml;bersicht</a>
		<br class="clearer" />
	</div>

</div>

<?php get_footer(); ?>