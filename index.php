<?php
/**
 * The main template file, used for Index Page (excerpts) of "aktuelles." page.
 *
 */

get_header(); ?>

<div class="page-blog">
	<h3>Frisch angemischt.</h3>
	<p>Aktuelles aus dem Studiobetrieb:</p>
	<div class="blogcontainer">
			<?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			?>
	</div>
<br class="clearer" />
</div>

<?php get_footer(); ?>