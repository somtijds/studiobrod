<div id="footer">
	<span class="footer">&copy; <?php echo date(Y);?> oliver brod</span>
	<a class="footer" href="mailto:post@studiobrod.com">post@studiobrod.com</a>
	<span class="footer">+49-177-2334738</span>
	<a class="footer" href="/impressum/">Impressum</a>
	<a class="footer" href="/links/">Links</a>
</div>
<div id="footerbottom"></div>

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
</body>
</html>