			<div class="o20_footer">
				<ul>
					<li class="footer-item"><span >&copy; <?php echo date(Y);?> studio brod</span></li>
					<li class="footer-item"><?php o20_email_address( 2, 'footer' ); ?></li>
					<li class="footer-item"><span >+49-177-2334738</span></li>
					<li class="footer-item"><a  href="/impressum/">Impressum</a></li>
					<li class="footer-item"><a  href="/links/">Links</a></li>
					<li class="footer-item footer-search"><?php get_search_form(); ?></li>
				</ul>
				</ul>
			</div>

		</div><!-- o20_container -->

		<?php
			/* Always have wp_footer() just before the closing </body>
			 * tag of your theme, or you will break many plugins, which
			 * generally use this hook to reference JavaScript files.
			 */
			wp_footer('search');
		?>

	</body>
</html>