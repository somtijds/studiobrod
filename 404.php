<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div class="page-blog">

	<div class="blogcontainer">
					<h3>Hm. Oder auch: &bdquo;Seite nicht gefunden.&ldquo;</h3>
					<p>Die gesuchte Seite existiert leider nicht oder nicht mehr, vielleicht hat der Link auch einen Tippfehler, es gibt ja heutzutage immer so viele M&ouml;glichkeiten.</p>
					<p>M&ouml;chten Sie <a href="/">zur Startseite</a>? Oder vielleicht hilft Ihnen die Suchfunktion:</p>
					<div><?php get_search_form(); ?></div>
	</div>

</div>

<?php get_footer('search'); ?>

