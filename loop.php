<?php
/**
 * The loop that displays posts.
 *
 */
?>


<?php while ( have_posts() ) : the_post(); ?>

			<h4 class="blog-index"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
	<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
			<div class="text-blogpost"><?php the_excerpt(); ?></div>
	<?php else : ?>
			<div class="blog-index-thumb"><?php the_post_thumbnail('thumbnail'); ?></div>
			<div class="blog-index-columnright">
			<div class="text-blogpost"><?php the_content( __( '&hellip; continue reading &raquo;', 'twentyten' ) ); ?></div>
			</div><br class="clearer" />
			<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'twentyten' ), 'after' => '' ) ); ?>
	<?php endif; ?>

<?php endwhile;?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<span class="entry-prev"><?php next_posts_link( __( '&laquo; Older posts', 'twentyten' ) ); ?></span>
				<span class="entry-next"><?php previous_posts_link( __( 'Newer posts &raquo;', 'twentyten' ) ); ?></span>
<?php endif; ?>