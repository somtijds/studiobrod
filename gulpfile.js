'use strict'

// Grab our gulp packages
var gulp  = require('gulp')
    , sass = require('gulp-sass')
    , postcss = require('gulp-postcss')
    , sourcemaps = require('gulp-sourcemaps')
    , jshint = require('gulp-jshint')
    , stylish = require('jshint-stylish')
    , uglify = require('gulp-uglify')
    , concat = require('gulp-concat')
    , rename = require('gulp-rename')
    , plumber = require('gulp-plumber');

// Compile Sass, Autoprefix and minify
gulp.task('styles', function() {
    return gulp.src('./assets/scss/**/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init()) // Start Sourcemaps
        .pipe(sass({}).on('error', sass.logError))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(postcss(
            [ require('precss'), require('autoprefixer'), require('cssnano') ]
            ))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.')) // Creates sourcemaps for minified styles
        .pipe(gulp.dest('./assets/css/'))
});

// JSHint, concat, and minify JavaScript
gulp.task('scripts', function() {
    return gulp.src([
            './assets/js/src/init.js'
            ])
        .pipe(plumber())
        .pipe(sourcemaps.init()) // Start Sourcemaps
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        //.pipe(concat('scripts.js'))
        .pipe(gulp.dest('./assets/js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write('.')) // Creates sourcemap for minified JS
        .pipe(gulp.dest('./assets/js'))
});

// Watch files for changes
gulp.task('watch', function() {
  gulp.watch('./assets/scss/**/*.scss', ['styles']); // Watch sass files
  gulp.watch('./assets/js/src/*.js', ['scripts']); // Watch script source files
});

// Run styles and scripts tasks by default
gulp.task('default', function() {
    gulp.start( 'styles', 'scripts' );
});
